package com.windows;

public class Grocery extends javax.swing.JFrame {

  
    public Grocery() {
        initComponents();
        this.setLocationRelativeTo(null);
    }

    private void initComponents() {

        catalogue = new javax.swing.JPanel();
        catalogueTxt = new javax.swing.JLabel();
        hygieneTxt = new javax.swing.JLabel();
        beverageTxt = new javax.swing.JLabel();
        groceyTxt = new javax.swing.JLabel();
        groceryButton = new javax.swing.JButton();
        hygieneButton = new javax.swing.JButton();
        beverageButton = new javax.swing.JButton();
        viewShoppingCartButton = new javax.swing.JButton();
        groceryBackButton = new javax.swing.JButton();
        groceyBackground = new javax.swing.JLabel();
        backgroundGrey = new javax.swing.JPanel();
        header = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        catalogue.setBackground(new java.awt.Color(34, 34, 34));
        catalogue.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        catalogueTxt.setFont(new java.awt.Font("Bebas Neue", 1, 36));
        catalogueTxt.setForeground(new java.awt.Color(255, 204, 0));
        catalogueTxt.setText("catalogo");
        catalogue.add(catalogueTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 20, -1, -1));

        hygieneTxt.setFont(new java.awt.Font("Bebas Neue", 1, 36));
        hygieneTxt.setForeground(new java.awt.Color(255, 255, 255));
        hygieneTxt.setText("LIMPIEZA E HIGIENE");
        catalogue.add(hygieneTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 210, -1, -1));

        beverageTxt.setFont(new java.awt.Font("Bebas Neue", 1, 36));
        beverageTxt.setForeground(new java.awt.Color(255, 255, 255));
        beverageTxt.setText("bebidas");
        catalogue.add(beverageTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 300, -1, -1));

        groceyTxt.setFont(new java.awt.Font("Bebas Neue", 1, 36));
        groceyTxt.setForeground(new java.awt.Color(255, 255, 255));
        groceyTxt.setText("ABARROTES");
        catalogue.add(groceyTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 120, -1, -1));

        groceryButton.setBackground(new java.awt.Color(255, 255, 255));
        groceryButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/imagenes/fondo343434.png")));
        groceryButton.setText("jButton1");
        groceryButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                groceryButtonActionPerformed(evt);
            }
        });
        catalogue.add(groceryButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 96, 390, 86));

        hygieneButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/imagenes/backgroundark.png")));
        hygieneButton.setText("jButton2");
        hygieneButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hygieneButtonActionPerformed(evt);
            }
        });
        catalogue.add(hygieneButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 188, 390, 87));

        beverageButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/imagenes/backgroundark.png")));
        beverageButton.setText("jButton3");
        beverageButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                beverageButtonActionPerformed(evt);
            }
        });
        catalogue.add(beverageButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 281, 390, 86));

        viewShoppingCartButton.setBackground(new java.awt.Color(255, 204, 0));
        viewShoppingCartButton.setFont(new java.awt.Font("Bebas Neue", 1, 36));
        viewShoppingCartButton.setForeground(new java.awt.Color(255, 255, 255));
        viewShoppingCartButton.setText("VER CARRITO");
        viewShoppingCartButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewShoppingCartButtonActionPerformed(evt);
            }
        });
        catalogue.add(viewShoppingCartButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(27, 479, 324, 81));

        getContentPane().add(catalogue, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 160, -1, 610));

        groceryBackButton.setBackground(new java.awt.Color(0, 0, 0));
        groceryBackButton.setFont(new java.awt.Font("Bebas Neue", 1, 24));
        groceryBackButton.setForeground(new java.awt.Color(255, 255, 255));
        groceryBackButton.setText("INICIO");
        groceryBackButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                groceryBackButtonActionPerformed(evt);
            }
        });
        getContentPane().add(groceryBackButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(1250, 40, 90, -1));

        groceyBackground.setBackground(new java.awt.Color(34, 34, 34));
        groceyBackground.setMaximumSize(new java.awt.Dimension(1398, 766));
        groceyBackground.setMinimumSize(new java.awt.Dimension(1398, 766));
        groceyBackground.setPreferredSize(new java.awt.Dimension(1398, 766));
        getContentPane().add(groceyBackground, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1398, 766));

        backgroundGrey.setBackground(new java.awt.Color(51, 51, 51));
        backgroundGrey.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        getContentPane().add(backgroundGrey, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 160, 1010, 610));

        header.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/imagenes/barraarriba.png")));
        header.setText("jLabel1");
        getContentPane().add(header, new org.netbeans.lib.awtextra.AbsoluteConstraints(-270, -20, 1670, 200));

        pack();
    }

    private void groceryBackButtonActionPerformed(java.awt.event.ActionEvent evt) {
        Home home = new Home();
        
        home.setVisible(true);
        
        this.dispose();
    }

    private void groceryButtonActionPerformed(java.awt.event.ActionEvent evt) {

    }

    private void hygieneButtonActionPerformed(java.awt.event.ActionEvent evt) {
        Hygiene hygiene = new Hygiene();

        hygiene.setVisible(true);

        this.dispose();
    }

    private void beverageButtonActionPerformed(java.awt.event.ActionEvent evt) {
        Beverage beverage = new Beverage();

        beverage.setVisible(true);

        this.dispose();
    }
    private void viewShoppingCartButtonActionPerformed(java.awt.event.ActionEvent evt) {
        ShoppingCart cart = new ShoppingCart();

        cart.setVisible(true);

        this.dispose();
    }

    private javax.swing.JPanel backgroundGrey;
    private javax.swing.JButton beverageButton;
    private javax.swing.JLabel beverageTxt;
    private javax.swing.JPanel catalogue;
    private javax.swing.JLabel catalogueTxt;
    private javax.swing.JButton groceryBackButton;
    private javax.swing.JButton groceryButton;
    private javax.swing.JLabel groceyBackground;
    private javax.swing.JLabel groceyTxt;
    private javax.swing.JLabel header;
    private javax.swing.JButton hygieneButton;
    private javax.swing.JLabel hygieneTxt;
    private javax.swing.JButton viewShoppingCartButton;
}
