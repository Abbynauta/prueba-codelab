package com.windows;

public class HomeRegister extends javax.swing.JFrame {

   
    public HomeRegister() {
        initComponents();
        this.setLocationRelativeTo(null);     
    }
    private void initComponents() {

        fullScreen = new javax.swing.JPanel();
        leftBackground = new javax.swing.JPanel();
        userData = new javax.swing.JTextField();
        passwordData = new javax.swing.JPasswordField();
        cellphoneTXT = new javax.swing.JLabel();
        passwordTXT = new javax.swing.JLabel();
        logo = new javax.swing.JLabel();
        startSessionButton = new javax.swing.JButton();
        registerButton = new javax.swing.JButton();
        userButton = new javax.swing.JRadioButton();
        adminButton = new javax.swing.JRadioButton();
        phraseBackground = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(40, 40, 40));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        fullScreen.setBackground(new java.awt.Color(255, 250, 199));
        fullScreen.setLayout(null);

        leftBackground.setBackground(new java.awt.Color(34, 34, 34));
        leftBackground.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        userData.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        userData.setText("Ingresa tu numero de celular para registrarte o iniciar sesion");
        userData.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                userDataActionPerformed(evt);
            }
        });
        leftBackground.add(userData, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 260, 550, 70));

        passwordData.setText("jPasswordField1");
        passwordData.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                passwordDataActionPerformed(evt);
            }
        });
        leftBackground.add(passwordData, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 410, 550, 70));

        cellphoneTXT.setFont(new java.awt.Font("Bebas Neue", 1, 24));
        cellphoneTXT.setForeground(new java.awt.Color(255, 204, 0));
        cellphoneTXT.setText("celular");
        leftBackground.add(cellphoneTXT, new org.netbeans.lib.awtextra.AbsoluteConstraints(109, 209, -1, -1));

        passwordTXT.setFont(new java.awt.Font("Bebas Neue", 1, 24));
        passwordTXT.setForeground(new java.awt.Color(255, 204, 0));
        passwordTXT.setText("CONTRASEÑA");
        leftBackground.add(passwordTXT, new org.netbeans.lib.awtextra.AbsoluteConstraints(109, 362, -1, -1));

        logo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/imagenes/Logo.png")));
        leftBackground.add(logo, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 0, 540, 170));

        startSessionButton.setBackground(new java.awt.Color(255, 204, 0));
        startSessionButton.setFont(new java.awt.Font("Bebas Neue", 1, 24));
        startSessionButton.setForeground(new java.awt.Color(102, 102, 102));
        startSessionButton.setText("iniciar sesion");
        startSessionButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startSessionButtonActionPerformed(evt);
            }
        });
        leftBackground.add(startSessionButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 590, 170, 50));

        registerButton.setBackground(new java.awt.Color(255, 204, 0));
        registerButton.setFont(new java.awt.Font("Bebas Neue", 1, 24));
        registerButton.setForeground(new java.awt.Color(102, 102, 102));
        registerButton.setText("REGISTRARME");
        registerButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                registerButtonActionPerformed(evt);
            }
        });
        leftBackground.add(registerButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 590, 170, 50));

        userButton.setFont(new java.awt.Font("Dialog", 0, 14));
        userButton.setForeground(new java.awt.Color(255, 255, 255));
        userButton.setText("Usuario");
        leftBackground.add(userButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 510, 120, 30));

        adminButton.setFont(new java.awt.Font("Dialog", 0, 14));
        adminButton.setForeground(new java.awt.Color(255, 255, 255));
        adminButton.setText("Administrador");
        adminButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                adminButtonActionPerformed(evt);
            }
        });
        leftBackground.add(adminButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 510, -1, 30));

        fullScreen.add(leftBackground);
        leftBackground.setBounds(-10, 0, 760, 781);

        phraseBackground.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/imagenes/iniciofrase.png")));
        phraseBackground.setText("jLabel5");
        fullScreen.add(phraseBackground);
        phraseBackground.setBounds(750, 0, 730, 776);

        getContentPane().add(fullScreen, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1400, 770));

        pack();
    }

    private void startSessionButtonActionPerformed(java.awt.event.ActionEvent evt) {
        Home home = new Home();
        
        home.setVisible(true);
        
        this.dispose();
    }

    private void passwordDataActionPerformed(java.awt.event.ActionEvent evt) {

    }

    private void registerButtonActionPerformed(java.awt.event.ActionEvent evt) {

    }

    private void userDataActionPerformed(java.awt.event.ActionEvent evt) {

    }

    private void adminButtonActionPerformed(java.awt.event.ActionEvent evt) {

    }

    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }


        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new HomeRegister().setVisible(true);
            }
        });
    }

    private javax.swing.JRadioButton adminButton;
    private javax.swing.JLabel cellphoneTXT;
    private javax.swing.JPanel fullScreen;
    private javax.swing.JPanel leftBackground;
    private javax.swing.JLabel logo;
    private javax.swing.JPasswordField passwordData;
    private javax.swing.JLabel passwordTXT;
    private javax.swing.JLabel phraseBackground;
    private javax.swing.JButton registerButton;
    private javax.swing.JButton startSessionButton;
    private javax.swing.JRadioButton userButton;
    private javax.swing.JTextField userData;

}
