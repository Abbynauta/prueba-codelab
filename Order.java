package com.windows;

public class Order extends javax.swing.JFrame {

   
    public Order() {
        initComponents();
        this.setLocationRelativeTo(null);
    }

    private void initComponents() {

        orderBackButton = new javax.swing.JButton();
        confirmOrderButton = new javax.swing.JButton();
        orderBackground = new javax.swing.JPanel();
        header = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        orderBackButton.setBackground(new java.awt.Color(0, 0, 0));
        orderBackButton.setFont(new java.awt.Font("Bebas Neue", 1, 24));
        orderBackButton.setForeground(new java.awt.Color(255, 255, 255));
        orderBackButton.setText("ATRAS");
        orderBackButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                orderBackButtonActionPerformed(evt);
            }
        });
        getContentPane().add(orderBackButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(1250, 40, 90, -1));

        confirmOrderButton.setBackground(new java.awt.Color(0, 204, 0));
        confirmOrderButton.setFont(new java.awt.Font("Bebas Neue", 1, 24));
        confirmOrderButton.setText("CONFIRMAR PEDIDO");
        getContentPane().add(confirmOrderButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(1000, 650, 350, 70));

        orderBackground.setBackground(new java.awt.Color(255, 255, 255));
        getContentPane().add(orderBackground, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 160, 1400, 610));

        header.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/imagenes/barraarriba.png")));
        header.setText("jLabel1");
        getContentPane().add(header, new org.netbeans.lib.awtextra.AbsoluteConstraints(-270, -20, 1670, 200));

        pack();
    }

    private void orderBackButtonActionPerformed(java.awt.event.ActionEvent evt) {
        ShoppingCart framecarrito = new ShoppingCart();
        
        framecarrito.setVisible(true);
        
        this.dispose();
    }


    private javax.swing.JButton confirmOrderButton;
    private javax.swing.JLabel header;
    private javax.swing.JButton orderBackButton;
    private javax.swing.JPanel orderBackground;

}
