package com.windows;

import java.awt.*;

public class Home extends javax.swing.JFrame {

    public Home() {
        initComponents();

        this.setLocationRelativeTo(null);
    }

    private void initComponents() {

        searchEngine = new javax.swing.JTextField();
        closeButton = new javax.swing.JButton();
        whiteBackground = new javax.swing.JPanel();
        catalogue = new javax.swing.JPanel();
        catalogueTxt = new javax.swing.JLabel();
        hygieneTxt = new javax.swing.JLabel();
        beverageTxt = new javax.swing.JLabel();
        groceyTxt = new javax.swing.JLabel();
        groceryButton = new javax.swing.JButton();
        hygieneButton = new javax.swing.JButton();
        beverageButton = new javax.swing.JButton();
        viewShoppingCartButton = new javax.swing.JButton();
        addButtonSix = new javax.swing.JButton();
        addButtonFIvve = new javax.swing.JButton();
        addButtonFour = new javax.swing.JButton();
        addButtonThree = new javax.swing.JButton();
        addButtonTwo = new javax.swing.JButton();
        addButtonOne = new javax.swing.JButton();
        addProductOne = new javax.swing.JButton();
        addProductTwo = new javax.swing.JButton();
        addProductThree = new javax.swing.JButton();
        addProductFour = new javax.swing.JButton();
        addProductFive = new javax.swing.JButton();
        productPriceOneTxt = new javax.swing.JLabel();
        productPriceTwoTxt = new javax.swing.JLabel();
        productPriceThreeTxt = new javax.swing.JLabel();
        productPriceFourTxt = new javax.swing.JLabel();
        productPriceFiveTxt = new javax.swing.JLabel();
        bestPricesTxt = new javax.swing.JLabel();
        productsUp = new javax.swing.JLabel();
        recommendedTxt = new javax.swing.JLabel();
        productsDown = new javax.swing.JLabel();
        header = new javax.swing.JLabel();
        background = new javax.swing.JLabel();
        logoHome = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("BIENVENIDO A KWIK-S-MART");
        setMinimumSize(new java.awt.Dimension(1398, 766));
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        searchEngine.setFont(new java.awt.Font("Dialog", Font.BOLD, 18));
        searchEngine.setForeground(new java.awt.Color(102, 102, 102));
        searchEngine.setText("Busca lo que necesites aqui...");
        getContentPane().add(searchEngine, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 30, 590, 60));

        closeButton.setBackground(new java.awt.Color(0, 0, 0));
        closeButton.setFont(new java.awt.Font("Bebas Neue", Font.BOLD, 24));
        closeButton.setForeground(new java.awt.Color(255, 204, 0));
        closeButton.setText("CERRAR SESION");
        closeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeButtonActionPerformed(evt);
            }
        });
        getContentPane().add(closeButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(1200, 50, -1, -1));

        whiteBackground.setBackground(new java.awt.Color(255, 255, 255));
        whiteBackground.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        catalogue.setBackground(new java.awt.Color(34, 34, 34));
        catalogue.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        catalogueTxt.setFont(new java.awt.Font("Bebas Neue", Font.BOLD, 36));
        catalogueTxt.setForeground(new java.awt.Color(255, 204, 0));
        catalogueTxt.setText("catalogo");
        catalogue.add(catalogueTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 20, -1, -1));

        hygieneTxt.setFont(new java.awt.Font("Bebas Neue", Font.BOLD, 36));
        hygieneTxt.setForeground(new java.awt.Color(255, 255, 255));
        hygieneTxt.setText("LIMPIEZA E HIGIENE");
        catalogue.add(hygieneTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 210, -1, -1));

        beverageTxt.setFont(new java.awt.Font("Bebas Neue", 1, 36));
        beverageTxt.setForeground(new java.awt.Color(255, 255, 255));
        beverageTxt.setText("bebidas");
        catalogue.add(beverageTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 300, -1, -1));

        groceyTxt.setFont(new java.awt.Font("Bebas Neue", 1, 36));
        groceyTxt.setForeground(new java.awt.Color(255, 255, 255));
        groceyTxt.setText("ABARROTES");
        catalogue.add(groceyTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 120, -1, -1));

        groceryButton.setBackground(new java.awt.Color(255, 255, 255));
        groceryButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/imagenes/fondo343434.png")));
        groceryButton.setText("jButton1");
        groceryButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                groceryButtonActionPerformed(evt);
            }
        });
        catalogue.add(groceryButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 96, 390, 86));

        hygieneButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/imagenes/fondo343434.png")));
        hygieneButton.setText("jButton2");
        hygieneButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hygieneButtonActionPerformed(evt);
            }
        });
        catalogue.add(hygieneButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 188, 390, 87));

        beverageButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/imagenes/fondo343434.png")));
        beverageButton.setText("jButton3");
        beverageButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                beverageButtonActionPerformed(evt);
            }
        });
        catalogue.add(beverageButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 281, 390, 86));

        viewShoppingCartButton.setBackground(new java.awt.Color(255, 204, 0));
        viewShoppingCartButton.setFont(new java.awt.Font("Bebas Neue", 1, 36));
        viewShoppingCartButton.setForeground(new java.awt.Color(255, 255, 255));
        viewShoppingCartButton.setText("VER CARRITO");
        viewShoppingCartButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewShoppingCartButtonActionPerformed(evt);
            }
        });
        catalogue.add(viewShoppingCartButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(27, 479, 324, 81));

        whiteBackground.add(catalogue, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, 610));

        addButtonSix.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/imagenes/botonmas4040.png")));
        addButtonSix.setContentAreaFilled(false);
        addButtonSix.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        whiteBackground.add(addButtonSix, new org.netbeans.lib.awtextra.AbsoluteConstraints(1070, 450, -1, -1));

        addButtonFIvve.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/imagenes/botonmas4040.png")));
        addButtonFIvve.setContentAreaFilled(false);
        addButtonFIvve.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        whiteBackground.add(addButtonFIvve, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 450, -1, -1));

        addButtonFour.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/imagenes/botonmas4040.png")));
        addButtonFour.setContentAreaFilled(false);
        addButtonFour.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        whiteBackground.add(addButtonFour, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 450, -1, -1));

        addButtonThree.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/imagenes/botonmas4040.png")));
        addButtonThree.setContentAreaFilled(false);
        addButtonThree.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        whiteBackground.add(addButtonThree, new org.netbeans.lib.awtextra.AbsoluteConstraints(1070, 330, -1, -1));

        addButtonTwo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/imagenes/botonmas4040.png")));
        addButtonTwo.setContentAreaFilled(false);
        addButtonTwo.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        whiteBackground.add(addButtonTwo, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 330, -1, -1));

        addButtonOne.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/imagenes/botonmas4040.png")));
        addButtonOne.setContentAreaFilled(false);
        addButtonOne.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        whiteBackground.add(addButtonOne, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 330, -1, -1));

        addProductOne.setBackground(new java.awt.Color(255, 204, 0));
        addProductOne.setFont(new java.awt.Font("Bebas Neue", 1, 18));
        addProductOne.setForeground(new java.awt.Color(51, 51, 51));
        addProductOne.setText("AGREGAR AL CARRITO");
        whiteBackground.add(addProductOne, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 240, 160, -1));

        addProductTwo.setBackground(new java.awt.Color(255, 204, 0));
        addProductTwo.setFont(new java.awt.Font("Bebas Neue", 1, 18));
        addProductTwo.setForeground(new java.awt.Color(51, 51, 51));
        addProductTwo.setText("AGREGAR AL CARRITO");
        whiteBackground.add(addProductTwo, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 240, 160, -1));

        addProductThree.setBackground(new java.awt.Color(255, 204, 0));
        addProductThree.setFont(new java.awt.Font("Bebas Neue", 1, 18));
        addProductThree.setForeground(new java.awt.Color(51, 51, 51));
        addProductThree.setText("AGREGAR AL CARRITO");
        whiteBackground.add(addProductThree, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 240, 160, -1));

        addProductFour.setBackground(new java.awt.Color(255, 204, 0));
        addProductFour.setFont(new java.awt.Font("Bebas Neue", 1, 18));
        addProductFour.setForeground(new java.awt.Color(51, 51, 51));
        addProductFour.setText("AGREGAR AL CARRITO");
        whiteBackground.add(addProductFour, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 240, 160, -1));

        addProductFive.setBackground(new java.awt.Color(255, 204, 0));
        addProductFive.setFont(new java.awt.Font("Bebas Neue", 1, 18));
        addProductFive.setForeground(new java.awt.Color(51, 51, 51));
        addProductFive.setText("AGREGAR AL CARRITO");
        addProductFive.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addProductFiveActionPerformed(evt);
            }
        });
        whiteBackground.add(addProductFive, new org.netbeans.lib.awtextra.AbsoluteConstraints(1130, 240, 160, -1));

        productPriceOneTxt.setFont(new java.awt.Font("Bebas Neue", 1, 30));
        productPriceOneTxt.setForeground(new java.awt.Color(255, 255, 255));
        productPriceOneTxt.setText("2$");
        whiteBackground.add(productPriceOneTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 100, 60, 40));

        productPriceTwoTxt.setFont(new java.awt.Font("Bebas Neue", 1, 30));
        productPriceTwoTxt.setForeground(new java.awt.Color(255, 255, 255));
        productPriceTwoTxt.setText("2$");
        whiteBackground.add(productPriceTwoTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 100, 60, 40));

        productPriceThreeTxt.setFont(new java.awt.Font("Bebas Neue", 1, 30));
        productPriceThreeTxt.setForeground(new java.awt.Color(255, 255, 255));
        productPriceThreeTxt.setText("2$");
        whiteBackground.add(productPriceThreeTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(890, 100, 60, 40));

        productPriceFourTxt.setFont(new java.awt.Font("Bebas Neue", 1, 30));
        productPriceFourTxt.setForeground(new java.awt.Color(255, 255, 255));
        productPriceFourTxt.setText("2$");
        whiteBackground.add(productPriceFourTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(1060, 100, 60, 40));

        productPriceFiveTxt.setFont(new java.awt.Font("Bebas Neue", 1, 30));
        productPriceFiveTxt.setForeground(new java.awt.Color(255, 255, 255));
        productPriceFiveTxt.setText("2$");
        whiteBackground.add(productPriceFiveTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(1250, 100, 60, 40));

        bestPricesTxt.setFont(new java.awt.Font("Bebas Neue", 1, 36));
        bestPricesTxt.setForeground(new java.awt.Color(0, 0, 0));
        bestPricesTxt.setText("LOS MEJORES PRECIOS!");
        whiteBackground.add(bestPricesTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 10, -1, -1));

        productsUp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/imagenes/Productosbuenprecio.png")));
        productsUp.setText("jLabel1");
        whiteBackground.add(productsUp, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 70, 1170, 200));

        recommendedTxt.setFont(new java.awt.Font("Bebas Neue", 1, 24));
        recommendedTxt.setForeground(new java.awt.Color(0, 0, 0));
        recommendedTxt.setText("recomendado                                                                 lo ultimo                                                                  populares                            ");
        whiteBackground.add(recommendedTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 300, 960, -1));

        productsDown.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/imagenes/PRODUCTOSINICIOABAJO.png")));
        productsDown.setText("jLabel6");
        whiteBackground.add(productsDown, new org.netbeans.lib.awtextra.AbsoluteConstraints(-40, 290, 1400, 330));

        getContentPane().add(whiteBackground, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 160, 1400, 610));

        header.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/imagenes/barraarriba.png")));
        header.setText("jLabel1");
        getContentPane().add(header, new org.netbeans.lib.awtextra.AbsoluteConstraints(-270, -20, 1670, 200));

        background.setMaximumSize(new java.awt.Dimension(1398, 766));
        background.setMinimumSize(new java.awt.Dimension(1398, 766));
        background.setPreferredSize(new java.awt.Dimension(1398, 766));
        getContentPane().add(background, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, 770));

        javax.swing.GroupLayout logoHomeLayout = new javax.swing.GroupLayout(logoHome);
        logoHome.setLayout(logoHomeLayout);
        logoHomeLayout.setHorizontalGroup(
            logoHomeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 390, Short.MAX_VALUE)
        );
        logoHomeLayout.setVerticalGroup(
            logoHomeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        getContentPane().add(logoHome, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        pack();
    }

    private void beverageButtonActionPerformed(java.awt.event.ActionEvent evt) {
        Beverage bevarage = new Beverage();
        
        bevarage.setVisible(true);
        
        this.dispose(); 
    }

    private void viewShoppingCartButtonActionPerformed(java.awt.event.ActionEvent evt) {
        ShoppingCart cart = new ShoppingCart();
        
        cart.setVisible(true);
        
        this.dispose(); 
    }

    private void closeButtonActionPerformed(java.awt.event.ActionEvent evt) {
        HomeRegister homeregister = new HomeRegister();
        
        homeregister.setVisible(true);
        
        this.dispose();
    }

    private void groceryButtonActionPerformed(java.awt.event.ActionEvent evt) {
        Grocery grocery = new Grocery();
        grocery.setVisible(true);
        
        this.dispose();     
    }

    private void hygieneButtonActionPerformed(java.awt.event.ActionEvent evt) {
        Hygiene hygiene = new Hygiene();
        
        hygiene.setVisible(true);
        
        this.dispose();  
    }

    private void addProductFiveActionPerformed(java.awt.event.ActionEvent evt) {
    }


    private javax.swing.JButton addButtonFIvve;
    private javax.swing.JButton addButtonFour;
    private javax.swing.JButton addButtonOne;
    private javax.swing.JButton addButtonSix;
    private javax.swing.JButton addButtonThree;
    private javax.swing.JButton addButtonTwo;
    private javax.swing.JButton addProductFive;
    private javax.swing.JButton addProductFour;
    private javax.swing.JButton addProductOne;
    private javax.swing.JButton addProductThree;
    private javax.swing.JButton addProductTwo;
    private javax.swing.JLabel background;
    private javax.swing.JLabel bestPricesTxt;
    private javax.swing.JButton beverageButton;
    private javax.swing.JLabel beverageTxt;
    private javax.swing.JPanel catalogue;
    private javax.swing.JLabel catalogueTxt;
    private javax.swing.JButton closeButton;
    private javax.swing.JButton groceryButton;
    private javax.swing.JLabel groceyTxt;
    private javax.swing.JLabel header;
    private javax.swing.JButton hygieneButton;
    private javax.swing.JLabel hygieneTxt;
    private javax.swing.JPanel logoHome;
    private javax.swing.JLabel productPriceFiveTxt;
    private javax.swing.JLabel productPriceFourTxt;
    private javax.swing.JLabel productPriceOneTxt;
    private javax.swing.JLabel productPriceThreeTxt;
    private javax.swing.JLabel productPriceTwoTxt;
    private javax.swing.JLabel productsDown;
    private javax.swing.JLabel productsUp;
    private javax.swing.JLabel recommendedTxt;
    private javax.swing.JTextField searchEngine;
    private javax.swing.JButton viewShoppingCartButton;
    private javax.swing.JPanel whiteBackground;

}
